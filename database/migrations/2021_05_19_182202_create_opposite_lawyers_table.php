<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOppositeLawyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opposite_lawyers', function (Blueprint $table) {
            $table->id();
      

            $table->string('name',100);
            $table->integer('mobile')->nullable();
            $table->string('email',20)->nullable();
            $table->string('description',200)->nullable();

            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opposite_lawyers');
    }
}
